=== Career Section ===
Contributors: shahinurislam
Donate link: https://forms.gle/EAtaCDDDxhcU5fva7
Tags: Install and send your CV.
Requires at least: 5.8
Tested up to: 6.4.2
Stable tag: 1.3
Requires PHP: 7.4
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Just another Career Section plugins. Simple but flexible.

== Description ==

Career Section shortcode for WordPress and easy to install. Take full control over your WordPress site, build any shortcode paste you can imagine – no programming knowledge required.

= Docs & Support =

You can find [docs](https://gitlab.com/shahinurislam/career-section), [FAQ](https://gitlab.com/shahinurislam/career-section) and more detailed information about career-section on [gitlab](https://gitlab.com/shahinurislam/career-section). If you were unable to find the answer to your question on the FAQ or in any of the documentation, you should check the [support forum](https://wordpress.org/support/plugin/career-section) on WordPress.org. If you can't locate any topics that pertain to your particular issue, post a new topic for it.

== Why Use Career Section? ==
Career Section gives you all the features needed to job listed without any hassle.

https://youtu.be/970xeR1wZpk

= Career Section Needs Your Support =

It is hard to continue development and support for this free plugin without contributions from users like you. If you enjoy using Career Section and find it useful, please consider [__making a donation__](https://forms.gle/EAtaCDDDxhcU5fva7). Your donation will help encourage and support the plugin's continued development and better user support. Find on gitlab. [Gitlab](https://gitlab.com/shahinurislam/Career-section) 

== Installation ==

1. Upload the entire `career-section` folder to the `/wp-content/plugins/` directory.
1. Activate the plugin through the 'Plugins' menu in WordPress.

You will find 'Contact' menu in your WordPress admin panel.

For basic usage, you can also have a look at the [plugin web site](https://gitlab.com/shahinurislam/career-section).

== Frequently Asked Questions ==

Do you have questions or issues with Career Section? Use these support channels appropriately.

= Career Section is responsive? =

Yes this plugin are fully responsive and support every mobile device.

= What about this plugin? =

This plugin is Career Section of any wp sites.

== Upgrade Notice ==

This is new version 1.3

== Screenshots ==

1. screenshot-1.png 
2. screenshot-2.png 
3. screenshot-3.png 
4. screenshot-4.png 

== Changelog ==

= 1.3 =

* Update WordPress Version.
* Add Email Recipient email.
* Bug Fix.

= 1.02 =

* Update.

= 1.01 =

* Update bootstrap.
* Bug Fix.
 
= 1.0 =

* Add new post type.
* Generate shortcode place and show the main file.
